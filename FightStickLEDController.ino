#include <stdlib.h>
#include <Time.h>

#define PI 3.14159265358979323846
#define TOTAL_COLOR_STEPS 255

#define RED 0
#define GREEN 1
#define BLUE 2
#define PURPLE 3
#define AQUA 4
#define YELLOW 5
#define WHITE 6
#define NONE 7
#define PULSERED 8
#define PULSEGREEN 9
#define PULSEBLUE 10
#define PULSEPURPLE 11
#define PULSEAQUA 12
#define PULSEYELLOW 13
#define PULSEWHITE 14
#define RAINBOW 15
#define PULSERAINBOW 16

const int LRedPin = 0;         //Light Punch/Kick Red LED Pin
const int LGrnPin = 1;         //Light Punch/Kick Grn LED Pin
const int LBluPin = 2;         //Light Punch/Kick Blu LED Pin
const int LWhtPin = 3;         //Light Punch/Kick Wht LED Pin
const int MRedPin = 4;         //Medium Punch/Kick Red LED Pin
const int MGrnPin = 5;         //Medium Punch/Kick Grn LED Pin
const int MBluPin = 6;         //Medium Punch/Kick Blu LED Pin
const int MWhtPin = 7;         //Medium Punch/Kick Wht LED Pin
const int HRedPin = 8;         //Heavy Punch/Kick Red LED Pin
const int HGrnPin = 9;         //Heavy Punch/Kick Grn LED Pin
const int HBluPin = 10;        //Heavy Punch/Kick Blu LED Pin
const int HWhtPin = 11;        //Heavy Punch/Kick Wht LED Pin
const int ARedPin = 12;        //All Punches/Kicks Red LED Pin
const int AGrnPin = 13;        //All Punches/Kicks Grn LED Pin
const int ABluPin = 14;        //All Punches/Kicks Blu LED Pin
const int AWhtPin = 15;        //All Punches/Kicks Wht LED Pin

const int shortPin = 16;       //Short Button Signal Pin 
const int jabPin = 17;         //Jab Button Signal Pin
const int forwardPin = 18;     //Forward Button Signal Pin
const int strongPin = 19;      //Strong Button Signal Pin
const int roundhousePin = 20;  //Roundhouse Button Signal Pin
const int fiercePin = 21;      //Fierce Button Signal Pin
const int tripleKPin = 22;     //All Kicks Button Signal Pin
const int triplePPin = 23;     //All Punches Button Signal Pin

const int rowSelectPinOne = 28;   //Controls which row is displaying LEDs (0 == kicks, 1 == punches)
const int rowSelectPinTwo = 29;   //Controls which row is displaying LEDs (1 == kicks, 0 == punches)

int currentRow;                //Stores current row which is displaying LEDs (0 == kicks, 1 == punches)
int currentScheme;             //Stores current lighting scheme (0 = lit always + flash white pushed, 1 = lights color when pushed, 2 = rainbow)
int rainbowCount;              //Counts each rainbow color change step
int rainbowTimer;              //Times each rainbow color change step
int rainbowTimerMax;           //Max of rainbowTimer which is randomly set
bool fastRainbow;              //Rainbow
int rainbowRandomCount;        //Counts until rainbow speed change
int flashCounter;              //Counter for flash duration
boolean schemeSwitched;        //Makes sure when scheme is switched, it isn't switched again until switching button combo is pushed again
boolean flashing;              //Is white flashing on for button(s)?
boolean mirrored;              //Is the BAM being mirrored?

uint8_t buttonLEDState[2][4][3];   //LED Values of the buttons
int colorMap[255][3];          //stores color change between red, green, and blue
boolean buttonState[2][4];     //current pushed state of the buttons

unsigned int stepsPassed;

// initializes values
void setup()
{  
  stepsPassed = 0;
  currentRow = 0;
  currentScheme = 2;
  flashCounter = 0;
  schemeSwitched = false;
  rainbowRandomCount = 500;
  flashing = false;
  mirrored = false;
  fastRainbow = false;
  
  for(int i = 0; i < 2; i++)
    for(int j = 0; j < 4; j++)
      buttonState[i][j] = false;
  
  for(int i = 0; i < 2; i++)
    for(int j = 0; j < 4; j++)
      for(int k = 0; k < 3; k++)
        buttonLEDState[i][j][k] = 0;
  
  pinMode(LRedPin, OUTPUT);
  pinMode(LGrnPin, OUTPUT);
  pinMode(LBluPin, OUTPUT);
  pinMode(LWhtPin, OUTPUT);  
  pinMode(MRedPin, OUTPUT);
  pinMode(MGrnPin, OUTPUT);
  pinMode(MBluPin, OUTPUT);
  pinMode(MWhtPin, OUTPUT);  
  pinMode(HRedPin, OUTPUT);
  pinMode(HGrnPin, OUTPUT);
  pinMode(HBluPin, OUTPUT);
  pinMode(HWhtPin, OUTPUT);  
  pinMode(ARedPin, OUTPUT);
  pinMode(AGrnPin, OUTPUT);
  pinMode(ABluPin, OUTPUT);
  pinMode(AWhtPin, OUTPUT);
  
  digitalWrite(LRedPin, HIGH);
  digitalWrite(LGrnPin, HIGH);
  digitalWrite(LBluPin, HIGH);
  digitalWrite(LWhtPin, HIGH);  
  digitalWrite(MRedPin, HIGH);
  digitalWrite(MGrnPin, HIGH);
  digitalWrite(MBluPin, HIGH);
  digitalWrite(MWhtPin, HIGH);  
  digitalWrite(HRedPin, HIGH);
  digitalWrite(HGrnPin, HIGH);
  digitalWrite(HBluPin, HIGH);
  digitalWrite(HWhtPin, HIGH);  
  digitalWrite(ARedPin, HIGH);
  digitalWrite(AGrnPin, HIGH);
  digitalWrite(ABluPin, HIGH);
  digitalWrite(AWhtPin, HIGH);
  
  pinMode(shortPin, INPUT);
  pinMode(jabPin, INPUT);
  pinMode(forwardPin, INPUT);
  pinMode(strongPin, INPUT);  
  pinMode(roundhousePin, INPUT);
  pinMode(fiercePin, INPUT);
  pinMode(tripleKPin, INPUT);
  pinMode(triplePPin, INPUT);
  
  pinMode(rowSelectPinOne, OUTPUT);
  pinMode(rowSelectPinTwo, OUTPUT);
  
  digitalWrite(rowSelectPinOne, HIGH);
  digitalWrite(rowSelectPinTwo, LOW);
  
  randomSeed(analogRead(0));
  
  buildLEDVals();
}

// 
void loop()
{
  // top row is calculated
  readInputs();
  writeOutputs();
  //debugOutputs();
  displayOutputs();
  
  // increments values so bottom row is now active
  incrementVars();
  
  // bottom row is calculated
  readInputs();
  writeOutputs();
  //debugOutputs();
  displayOutputs();
  
  // cycles back to first row
  incrementVars();
}

// increments all LED color values
void incrementVars()
{
  
  // writes all pins to high by default
  digitalWrite(LRedPin, HIGH);
  digitalWrite(LGrnPin, HIGH);
  digitalWrite(LBluPin, HIGH);
  digitalWrite(LWhtPin, HIGH);
  digitalWrite(MRedPin, HIGH);
  digitalWrite(MGrnPin, HIGH);
  digitalWrite(MBluPin, HIGH);
  digitalWrite(MWhtPin, HIGH);
  digitalWrite(HRedPin, HIGH);
  digitalWrite(HGrnPin, HIGH);
  digitalWrite(HBluPin, HIGH);
  digitalWrite(HWhtPin, HIGH);
  digitalWrite(ARedPin, HIGH);
  digitalWrite(AGrnPin, HIGH);
  digitalWrite(ABluPin, HIGH);
  digitalWrite(AWhtPin, HIGH);
  
  currentRow++;
  currentRow = currentRow % 2;
  if(currentRow == 0)
  {
    stepsPassed++;
    delayMicroseconds(5);
  }
  stepsPassed = stepsPassed % 256;
  flashCounter++;
  if(flashCounter > 2000) {
    flashing = !flashing;
    flashCounter = 0;
  }
  rainbowTimer --;
  
  // calculates the current rainbow color of each button
  if(rainbowTimer <= 0)
  {
    rainbowRandomCount--;
    if(rainbowRandomCount <= 0){
      if(fastRainbow){
        rainbowTimerMax = random(50, 150);
        fastRainbow = false; 
        rainbowRandomCount = 1700;
      } else {
        rainbowTimerMax = random(250, 500); 
        fastRainbow = true;
        rainbowRandomCount = 500;
      }
    }
    rainbowCount++;
    rainbowCount = rainbowCount % 255;
    if(currentScheme == 2)
      for(int i = 0; i < 4; i++)
      {
        buttonLEDState[0][i][0] = colorMap[(rainbowCount + ((i + currentRow)*255) + 32 * i) % 255][0];
        buttonLEDState[0][i][1] = colorMap[(rainbowCount + ((i + currentRow)*255) + 32 * i) % 255][1];
        buttonLEDState[0][i][2] = colorMap[(rainbowCount + ((i + currentRow)*255) + 32 * i) % 255][2];
        buttonLEDState[1][i][0] = colorMap[(rainbowCount + ((i + currentRow)*255) + 32 * (i + 1)) % 255][0];
        buttonLEDState[1][i][1] = colorMap[(rainbowCount + ((i + currentRow)*255) + 32 * (i + 1)) % 255][1];
        buttonLEDState[1][i][2] = colorMap[(rainbowCount + ((i + currentRow)*255) + 32 * (i + 1)) % 255][2];
      }
    rainbowTimer = rainbowTimerMax;
  }
}

// writes the current LED array states to each pin
// uses mirrored bit angle modulation (coined BAMMAB) for more natural dimming/color changing
void displayOutputs()
{
  if(!mirrored)
  {
    if(stepsPassed >= 128)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if((buttonLEDState[currentRow][i][j] >> 7) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed >= 64)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 6) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed >= 32)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 5) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed >= 16)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 4) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed >= 8)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 3) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed >= 4)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 2) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed >= 2)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 1) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else 
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if((buttonLEDState[currentRow][i][j] % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
      mirrored = !mirrored;
    }
  }
  else
  {
    if(stepsPassed <= 1)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if((buttonLEDState[currentRow][i][j] % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed <= 3)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 1) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed <= 7)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 2) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed <= 15)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 3) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed <= 31)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 4) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed <= 63)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 5) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else if(stepsPassed <= 127)
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if(((buttonLEDState[currentRow][i][j] >> 6) % 2) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
    }
    else 
    {
      for(int i = 0; i < 4; i++)
      {
        for(int j = 0; j < 3; j++)
        {
          if((buttonLEDState[currentRow][i][j] >> 7) == 1)
            digitalWrite(i*4 + j, LOW);
          else
            digitalWrite(i*4 + j, HIGH);
        }
      }
      mirrored = !mirrored;
    }
  }
}

// reads inputs from buttons
void readInputs()
{
  for(int i = shortPin; i < triplePPin + 1; i++)
  {
    if(digitalRead(i) == LOW)
      buttonState[(i - shortPin) % 2][(i - shortPin) / 2] = true;
    else
      buttonState[(i - shortPin) % 2][(i - shortPin) / 2] = false;
  }
}

// write outputs to LEDs
void writeOutputs()
{
  if(currentRow == 0){
    digitalWrite(rowSelectPinOne, HIGH);
    digitalWrite(rowSelectPinTwo, LOW);
  } else{
    digitalWrite(rowSelectPinOne, LOW);
    digitalWrite(rowSelectPinTwo, HIGH);
  }
  
  if(buttonState[currentRow][0]) //short or jab
  {
    if(buttonState[(currentRow + 1) % 2][0]) //short and jab
    {
      if(buttonState[0][3] && buttonState[1][3] && !buttonState[0][1] && !buttonState[1][1]  && !buttonState[2][0]  && !buttonState[2][1] && !schemeSwitched) //press All Kicks, All Punches, Jab, and Short to change schemes
          changeSchemes();
      
      if(!buttonState[0][3] || !buttonState[1][3])
        schemeSwitched = false;
        
      switch(currentScheme)
      {
        case 0:
          writeColor(AQUA, LRedPin);
          if(flashing)
            digitalWrite(LWhtPin, HIGH);
          else
            digitalWrite(LWhtPin, LOW);
          break;
        case 1:
          writeColor(PURPLE, LRedPin);
          break;
      }
    }
    else //just short or jab
    {
      schemeSwitched = false;
      switch(currentScheme)
      {
        case 0:
          writeColor(AQUA, LRedPin);
          if(flashing && (currentRow == 0 && buttonState[0][0] || currentRow == 1 && buttonState[1][0]))
            digitalWrite(LWhtPin, HIGH);
          else
            digitalWrite(LWhtPin, LOW);
          break;
        case 1:
          writeColor(AQUA, LRedPin);
          break;
      }
    }
  }
  else //short or jab not pushed
  {
    schemeSwitched = false;
    switch(currentScheme)
    {
      case 0:
        writeColor(AQUA, LRedPin);
        digitalWrite(LWhtPin, HIGH);
        break;
      case 1:
        writeColor(NONE, LRedPin);
        break;
    }
  }
  
  if(buttonState[currentRow][1]) //forward or strong
  {
    if(buttonState[(currentRow + 1) % 2][1]) //forward and strong
    {
      switch(currentScheme)
      {
        case 0:
          writeColor(YELLOW, MRedPin);
          if(flashing)
            digitalWrite(MWhtPin, HIGH);
          else
            digitalWrite(MWhtPin, LOW);
          break;
        case 1:
          writeColor(WHITE, MRedPin);
          break;
      }
    }
    else //just forward or strong
    {
      switch(currentScheme)
      {
        case 0:
          writeColor(YELLOW, MRedPin);
          if(flashing)
            digitalWrite(MWhtPin, HIGH);
          else
            digitalWrite(MWhtPin, LOW);
          break;
        case 1:
          writeColor(YELLOW, MRedPin);
          break;
      }
    }
  }
  else //forward or strong not pushed
  {
    switch(currentScheme)
    {
      case 0:
        writeColor(YELLOW, MRedPin);
        digitalWrite(MWhtPin, HIGH);
        break;
      case 1:
        writeColor(NONE, MRedPin);
        break;
    }
  }
  
  if(buttonState[currentRow][2]) //roundhouse or fierce
  {
    if(buttonState[(currentRow + 1) % 2][2]) //roundhouse and fierce
    {
      switch(currentScheme)
      {
        case 0:
          writeColor(RED, HRedPin);
          if(flashing)
            digitalWrite(HWhtPin, HIGH);
          else
            digitalWrite(HWhtPin, LOW);
          break;
        case 1:
          writeColor(BLUE, HRedPin);
          break;
      }
    }
    else //just roundhouse or fierce
    {
      switch(currentScheme)
      {
        case 0:
          writeColor(RED, HRedPin);
          if(flashing)
            digitalWrite(HWhtPin, HIGH);
          else
            digitalWrite(HWhtPin, LOW);
          break;
        case 1:
          writeColor(RED, HRedPin);
          break;
      }
    }
  }
  else //roundhouse or fierce not pushed
  {
    switch(currentScheme)
    {
      case 0:
        writeColor(RED, HRedPin);
        digitalWrite(HWhtPin, HIGH);
        break;
      case 1:
        writeColor(NONE, HRedPin);
        break;
    }
  }
  
  if(buttonState[currentRow][3]) //all three kicks or all three punches
  {
    if(buttonState[(currentRow + 1) % 2][3]) //all three kicks and all three punches
    {
      if(buttonState[0][0] && buttonState[1][0] &&  !buttonState[0][1] && !buttonState[1][1]  && !buttonState[2][0]  && !buttonState[2][1] &&  !schemeSwitched) //press All Kicks, All Punches, Jab, and Short to change schemes
        changeSchemes();
        
      if(!buttonState[0][0] || !buttonState[1][0])
        schemeSwitched = false;
        
      switch(currentScheme)
      {
        case 0:
          writeColor(GREEN, ARedPin);
          if(flashing)
            digitalWrite(AWhtPin, HIGH);
          else
            digitalWrite(AWhtPin, LOW);
          break;
        case 1:
          writeColor(GREEN, ARedPin);
          break;
      }
    }
    else //just all three kicks or all three punches
    {
      schemeSwitched = false;
      switch(currentScheme)
      {
        case 0:
          writeColor(GREEN, ARedPin);
          if(flashing)
            digitalWrite(AWhtPin, HIGH);
          else
            digitalWrite(AWhtPin, LOW);
          break;
        case 1:
          writeColor(GREEN, ARedPin);
          break;
      }
    }
  }
  else //all three kicks or all three punches not pushed
  {
    schemeSwitched = false;
    switch(currentScheme)
    {
      case 0:
        writeColor(GREEN, ARedPin);
        digitalWrite(AWhtPin, HIGH);
        break;
      case 1:
        writeColor(NONE, ARedPin);
        break;
    }
  }
}

// write precalculated color to LED array
void writeColor(int color, int pin)
{
  switch(color)
  {
    case RED:
      buttonLEDState[currentRow][pin/4][0] = colorMap[0][0];
      buttonLEDState[currentRow][pin/4][1] = colorMap[0][1];
      buttonLEDState[currentRow][pin/4][2] = colorMap[0][2];
      break;
    case GREEN:
      buttonLEDState[currentRow][pin/4][0] = colorMap[85][0];
      buttonLEDState[currentRow][pin/4][1] = colorMap[85][1];
      buttonLEDState[currentRow][pin/4][2] = colorMap[85][2];
      break;
    case BLUE:
      buttonLEDState[currentRow][pin/4][0] = colorMap[170][0];
      buttonLEDState[currentRow][pin/4][1] = colorMap[170][1];
      buttonLEDState[currentRow][pin/4][2] = colorMap[170][2];
      break;
    case PURPLE:
      buttonLEDState[currentRow][pin/4][0] = colorMap[212][0];
      buttonLEDState[currentRow][pin/4][1] = colorMap[212][1];
      buttonLEDState[currentRow][pin/4][2] = colorMap[212][2];
      break;
    case AQUA:
      buttonLEDState[currentRow][pin/4][0] = colorMap[127][0];
      buttonLEDState[currentRow][pin/4][1] = colorMap[127][1];
      buttonLEDState[currentRow][pin/4][2] = colorMap[127][2];
      break;
    case YELLOW:
      buttonLEDState[currentRow][pin/4][0] = colorMap[42][0];
      buttonLEDState[currentRow][pin/4][1] = colorMap[42][1];
      buttonLEDState[currentRow][pin/4][2] = colorMap[42][2];
      break;
    case WHITE:
      buttonLEDState[currentRow][pin/4][0] = colorMap[0][0];
      buttonLEDState[currentRow][pin/4][1] = colorMap[85][1];
      buttonLEDState[currentRow][pin/4][2] = colorMap[170][2];
      break;
    case NONE:
      buttonLEDState[currentRow][pin/4][0] = 0;
      buttonLEDState[currentRow][pin/4][1] = 0;
      buttonLEDState[currentRow][pin/4][2] = 0;
      break;      
  }
}

// press All Kicks, All Punches, Jab, and Short to change schemes
void changeSchemes()
{
  currentScheme++;
  currentScheme = currentScheme % 3;
  schemeSwitched = true;
  digitalWrite(LRedPin, HIGH);
  digitalWrite(LGrnPin, HIGH);
  digitalWrite(LBluPin, HIGH);
  digitalWrite(LWhtPin, HIGH);
  digitalWrite(MRedPin, HIGH);
  digitalWrite(MGrnPin, HIGH);
  digitalWrite(MBluPin, HIGH);
  digitalWrite(MWhtPin, HIGH);
  digitalWrite(HRedPin, HIGH);
  digitalWrite(HGrnPin, HIGH);
  digitalWrite(HBluPin, HIGH);
  digitalWrite(HWhtPin, HIGH);
  digitalWrite(ARedPin, HIGH);
  digitalWrite(AGrnPin, HIGH);
  digitalWrite(ABluPin, HIGH);
  digitalWrite(AWhtPin, HIGH);
  
  if(currentScheme == 2)
  {
    rainbowCount = 0;
  }
}

// builds color map array which contains specific color values
void buildLEDVals() {
  float red, green, blue;
  int colorState = 0;
  for(int i = 0; i < TOTAL_COLOR_STEPS; i++) {
    if(i >= (colorState + 1) * TOTAL_COLOR_STEPS / 3)
      colorState++;
    
    float s = sin(PI * (3 * i - colorState * TOTAL_COLOR_STEPS) / (2 * TOTAL_COLOR_STEPS));
    float c = cos(PI * (3 * i - colorState * TOTAL_COLOR_STEPS) / (2 * TOTAL_COLOR_STEPS));
    //Serial.println("Phase: " + String(phase));
    
    red = 0;
    green = 0;
    blue = 0;
    
    if(colorState == 0) {
      green = 255 * s;
      red = 255 * c;
    }
    if(colorState == 1) {
      blue = 255 * s;
      green = 255 * c;
    }
    if(colorState == 2) {
      red = 255 * s;
      blue = 255 * c;
    }
    
    colorMap[i][RED] = (int)red;
    colorMap[i][GREEN] = (int)green;
    colorMap[i][BLUE] = (int)blue;
  }
}

// debug method for testing
void debugOutputs()
{
    writeColor(WHITE, LRedPin);
}

